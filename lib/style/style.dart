import 'package:flutter/material.dart';
class AppColors {
  static const AppBarColor = 0Xff303030;
    static const TabIconNormal = 0Xff999999;
    static const TabIconAction = 0Xff46c11b;
    static const AppBarPopupMenuColor = 0Xffffffff;
    static const TitleTextColor = 0Xff353535;
    static const DesTextColor = 0Xff9e9e9e;
    static const ConverstionItemBg = 0Xffffffff;
    static const DividerColor = 0Xffd9d9d9;
    static const NotifyDotBg = 0Xffff3e3e;
    static const NotifyDotText = 0Xffffffff;
    static const ConversationMuteIcon = 0Xffd8d8d8;
    static const DeviceInfoItemBg = 0Xfff5f5f5;
    static const DeviceInfoTtemText = 0Xff606062;

}
class AppStlyes {
  static const TitleStyle = TextStyle(
    fontSize: 14.0,
    color: Color(AppColors.TitleTextColor)
  );
  static const DesStyle = TextStyle(
    fontSize: 12.0,
    color: Color(AppColors.DesTextColor)
  );
  static const UnreadMsgCountDotStyle = TextStyle(
    fontSize: 12.0,
    color: Color(AppColors.NotifyDotText)
  );
  static const DeviceInfoTtemTextStyle = TextStyle(
    fontSize: 13.0,
    color: Color(AppColors.DesTextColor)
  );
}
class Constants {
    static const IconFontFamily = "myIcon";
    static const ConversationAvatarSize = 48.0;
    static const DividerHight = 1.0;
    static const UnReadMsgCountSize = 20.0;
     static const ConversationMuteIconSize = 18.0;
}