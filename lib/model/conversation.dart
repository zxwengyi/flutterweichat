import 'package:flutter/material.dart';
import '../style/style.dart' show AppColors;

enum Device {MAC, WIN}

class Conversation{
  const Conversation ({
    @required this.avatar,
    @required this.title,
    this.titleColor: AppColors.AppBarColor,
    this.des,
    @required this.creatAt,
    this.isMute: false,
    this.unreadMsgCout: 0,
    this.displayDot: false

  }) :assert(avatar != null),
      assert(creatAt != null),
      assert(avatar != null);

  final String avatar;
  final String title;
  final int titleColor;
  final String des;
  final String creatAt;
  final bool isMute;
  final int unreadMsgCout;
  final bool displayDot;

  bool isAvatarNetWork (){
    if(
      this.avatar.indexOf('http') == 0 || 
      this.avatar.indexOf('https') == 0
    ){
      return true;
    }
    return false;
  }

}
 const List<Conversation> mockConversations = [
   Conversation(avatar: 'images/chat/document.jpg', title: '文件传输助手', creatAt: '19:20', des: ''),
   Conversation(avatar: 'images/chat/public.jpg', title: '公众号', creatAt: '11:20', des: '广州移动'),
   Conversation(avatar: 'https://randomuser.me/api/portraits/men/67.jpg', title: 'weng', des: 'nihaodfd', creatAt: '1:20', unreadMsgCout: 2, isMute: true),
 ];
 const Map<String, List<Conversation>> mockConversationData = {
   'deviceInfo': null,
   'conversations': mockConversations
 };
 class ConversationPageData{
   const ConversationPageData({
     this.device,
     this.conversations
   });
   final Device device;
   final List<Conversation> conversations;

   static mock () {
     return ConversationPageData(device: Device.MAC, conversations: mockConversations);
   }
 }