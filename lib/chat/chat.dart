import 'package:flutter/material.dart';
import '../style/style.dart' show AppColors, AppStlyes, Constants;
import '../model/conversation.dart' show Conversation, ConversationPageData, Device;
class Conversationpage extends StatefulWidget {
  @override
  _ConversationpageState createState() => _ConversationpageState();
}
class _ConversationItem extends StatelessWidget {
  const _ConversationItem({Key key, this.conversation})
  : assert(conversation != null),
  super(key: key);

  final Conversation conversation;
  @override
  Widget build(BuildContext context) {
    Widget avatar, unReadMsgCountWidgit;
    conversation.isAvatarNetWork() ?
      avatar = Image.network(conversation.avatar,
          width: Constants.ConversationAvatarSize,
          height: Constants.ConversationAvatarSize,
         )
     :
     avatar = Image.asset(conversation.avatar,
          width: Constants.ConversationAvatarSize,
          height: Constants.ConversationAvatarSize,
         )
     ;
     unReadMsgCountWidgit = Container(
       width: Constants.UnReadMsgCountSize,
       height: Constants.UnReadMsgCountSize,
       alignment: Alignment.center,
       decoration: BoxDecoration(
         borderRadius: BorderRadius.circular(Constants.UnReadMsgCountSize/2.0),
         color: Color(AppColors.NotifyDotBg)
       ),
       child: Text(conversation.unreadMsgCout.toString(), style: AppStlyes.UnreadMsgCountDotStyle,),
     );
     Widget avatarContainer ;
     if(conversation.unreadMsgCout >0){
        avatarContainer = Stack(
        overflow: Overflow.visible,
        children: <Widget>[
          avatar, 
        Positioned(
          right: -Constants.UnReadMsgCountSize/2.0,
          top: -Constants.UnReadMsgCountSize/2.0, 
          child: unReadMsgCountWidgit,
        )
        ],
      );
     }else{
      avatarContainer = avatar;
     }

    //  请勿打扰
    var _rightView = <Widget>[
      Text(conversation.creatAt, style: AppStlyes.DesStyle,),
      SizedBox(height: 10.0,)

    ];
    if(conversation.isMute) {
      _rightView.add(
        Icon(IconData(
      0xe629,
      fontFamily: Constants.IconFontFamily,
      ), 
      color: Color(AppColors.ConversationMuteIcon),
      size: Constants.UnReadMsgCountSize)
      );
    }else{
      _rightView.add(
        Icon(IconData(
      0xe629,
      fontFamily: Constants.IconFontFamily,
      ), 
      color: Colors.transparent,
      size: Constants.UnReadMsgCountSize)
      );
    }

    return Container(
      padding: EdgeInsets.all(10.0),
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            color: Color(AppColors.DividerColor),
            width: Constants.DividerHight
          )
        )
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          avatarContainer,
         Container(width: 10.0,),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(conversation.title, style: AppStlyes.TitleStyle,),
                Text(conversation.des, style: AppStlyes.DesStyle,)
              ],
            )
          ),
          Container(
            child: Column(
              children: _rightView
            ),
          )
        ],
      ),
    );
  }
}
class _DeviceInfoItem extends StatelessWidget {
  const _DeviceInfoItem({this.device: Device.WIN})
  : assert (device != null);
  final Device device;
  int get IconName {
    return device == Device.WIN ? 0xe65b : 0xe640;
  }
  String get DeviceName {
    return device == Device.WIN ? "windos" : "mac";
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 24.0,top: 10.0,right: 24.0,bottom: 10.0),
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            width: Constants.DividerHight, color: Color(AppColors.DividerColor)
          ),
        ),
        color: Color(AppColors.DeviceInfoItemBg)
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Icon(IconData(
            this.IconName,
            fontFamily: Constants.IconFontFamily,
            ), size: 24.0,),
            Container(width: 16.0,),
            Text("$DeviceName 微信已登陆，手机通知以关闭")
        ],
      ),
    );
  }
}
class _ConversationpageState extends State<Conversationpage> {
  final ConversationPageData data = ConversationPageData.mock();
  @override
  Widget build(BuildContext context) {
    var mockConversations = data.conversations;
    return ListView.builder(
      itemBuilder: (BuildContext context, int index){
        if (data.device != null) {
          if(index == 0){
            return _DeviceInfoItem(device: data.device,);
          }
          return _ConversationItem(conversation: mockConversations[index-1],);
        }else{
          return _ConversationItem(conversation: mockConversations[index],);
        }
      },
      itemCount: data.device != null ? mockConversations.length+1 : mockConversations.length,
    );
  }
}