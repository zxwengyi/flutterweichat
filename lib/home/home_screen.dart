import 'package:flutter/material.dart';
import '../style/style.dart' show Constants, AppColors;
import '../chat/chat.dart';
enum ActionItem {GROUP_CHAT,ADD_FRIEND,QR_SCAN,PAYMENT,HELP}
class NavigationIconView {
  final BottomNavigationBarItem item;
  NavigationIconView({Key key, String title, IconData icon, IconData actionIcon}):
        item = BottomNavigationBarItem(
            title: Text(title),
            icon: Icon(icon),
            activeIcon: Icon(actionIcon),
            backgroundColor: Colors.white
        );
}

class HomeScreen extends StatefulWidget{
  _HomeScreenState createState() => new _HomeScreenState();
}
class _HomeScreenState extends State<HomeScreen>{
    
  PageController _pageController;
   int _currentIndex = 0;
  List<NavigationIconView> _navigationIconViews;
  List<Widget> _pages;
  void initState(){
    super.initState();
    _navigationIconViews = [
      NavigationIconView(
          title: '微信',
          icon: IconData(
            0xe6db,
            fontFamily: Constants.IconFontFamily,
          ),
          actionIcon: IconData(
            0xe615,
            fontFamily: Constants.IconFontFamily,
          )
      ),
      NavigationIconView(
          title: '通讯录',
          icon: IconData(
              0xe655,
              fontFamily: Constants.IconFontFamily,
          ),
          actionIcon: IconData(
               0xe6c2,
              fontFamily: Constants.IconFontFamily,
          )
      ),NavigationIconView(
          title: '发现',
          icon: IconData(
              0xe63b,
              fontFamily: Constants.IconFontFamily,
          ),
          actionIcon: IconData(
              0xe609,
              fontFamily: Constants.IconFontFamily,
          )
      ),
      NavigationIconView(
          title: '我',
          icon: IconData(
              0xe6c1,
              fontFamily: Constants.IconFontFamily,
          ),
          actionIcon: IconData(
               0xe61e,
              fontFamily: Constants.IconFontFamily,
          )
      )
    ];
   _pageController = PageController(initialPage: _currentIndex);
   _pages = [
     Conversationpage(),
     Container(color: Colors.green,),
     Container(color: Colors.grey,),
     Container(color: Colors.black,)
     ];
  }
  _buildPopupMunuItem (int iconName, String title) {
  return Row(
    children: <Widget>[
      Icon(IconData(
        iconName,
        fontFamily: Constants.IconFontFamily
      ),size: 22.0, color: Color(AppColors.AppBarPopupMenuColor),),
      Container(width: 12.0),
      Text(title, style: TextStyle(color: Color(AppColors.AppBarPopupMenuColor))),
    ],
  );
  }
 @override
  Widget build(BuildContext context) {
    final BottomNavigationBar _botNavBar = new BottomNavigationBar(
      fixedColor: const Color(AppColors.TabIconAction),
        items: _navigationIconViews.map(
            (NavigationIconView view) {
              return view.item;
            }
        ).toList(),
      currentIndex: _currentIndex,
      type: BottomNavigationBarType.fixed,
      onTap: (int index) {
       setState(() {
         _pageController.animateToPage(index, duration: Duration(microseconds: 200), curve: Curves.easeIn);
          _currentIndex = index;
       });
        print('点击了$index个bar');
      }
    );
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("微信"),
        elevation: 0.0,
        actions: <Widget>[
          IconButton(icon: Icon(IconData(
            0xe610,
            fontFamily: Constants.IconFontFamily,
          ),size: 22.0,),
          onPressed: (){
            print("点击了搜索");
          },
          ),
          Container(width: 16.0,),
          PopupMenuButton(itemBuilder: (BuildContext context){
            return <PopupMenuItem <ActionItem>>[
              PopupMenuItem(child: _buildPopupMunuItem( 0xe7da, '发起群聊'),
                value: ActionItem.GROUP_CHAT ,
              ),
               PopupMenuItem(child: _buildPopupMunuItem( 0xe8ca, '添加好友'),
                value: ActionItem.ADD_FRIEND ,
              ),
              PopupMenuItem(child: _buildPopupMunuItem( 0xe601, '扫一扫'),
                value: ActionItem.QR_SCAN ,
              ),
               PopupMenuItem(child: _buildPopupMunuItem( 0xe7da, '收付款'),
                value: ActionItem.PAYMENT ,
              ),
               PopupMenuItem(child: _buildPopupMunuItem( 0xe7da, '帮助与反馈'),
                value: ActionItem.HELP ,
              ),
            ];
          },
            icon: Icon(IconData(
              0xeb8f,
              fontFamily: Constants.IconFontFamily,
            ),size: 22.0,),
            onSelected: (ActionItem slected) {
               print('你点击了$slected');
            },
          ),
          Container(width: 24.0,),
        ],
      ),
      body: PageView.builder(itemBuilder: (BuildContext context, int index){
        return _pages[index];
      },
      controller: _pageController,
      itemCount: _pages.length,
      onPageChanged: (int index) {
        setState(() {
          _currentIndex = index;
        });
      },
      ),
      bottomNavigationBar: _botNavBar,
    );
  }
}