import 'package:flutter/material.dart';
import 'home/home_screen.dart';
import 'style/style.dart' show AppColors;
void main() => runApp(MaterialApp(
  title: '仿微信',
  theme: ThemeData.light().copyWith(
    primaryColor: Color(AppColors.AppBarColor),
    cardColor: Color(AppColors.AppBarColor)
  ),
  home: HomeScreen(),
));